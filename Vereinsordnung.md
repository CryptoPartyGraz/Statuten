Vereinsordnung des Vereins CryptoParty Graz
===========================================


§ 1: Organisationstreffen
-------------------------

(1) Das Organisationstreffen ist ein Treffen der Mitglieder. Alle Mitglieder können am Organisationstreffen teilnehmen. 
Stimmberechtigt sind Mitglieder die laut Statuten auch bei einer Generalversammlung stimmberechtigt wären.

(2) Es findet regulär ein Mal im Monat statt. Das Organisationstreffen wird vom Vorstand, spätestens am Vortag 
schriftlich angekündigt.

(3) Stimmberechtigte Mitglieder haben jeweils eine Stimme. Die Übertragung des Stimmrechts auf ein anderes Mitglied 
im Wege einer schriftlichen Bevollmächtigung ist nicht zulässig.

(4) Das Organisationstreffen ist ohne Rücksicht auf die Anzahl der Erschienenen beschlussfähig, wenn alle 
Mitglieder eine Ankündigung bekommen haben.

(5) Das Organisationstreffen fasst seine Beschlüsse mit einfacher Stimmenmehrheit der abgegebenen gültigen Stimmen.

(6) Vereinsordnungsänderungen müssen bei einem Organisationstreffen vorgestellt werden und dürfen erst beim nächsten 
regulären Organisationstreffen beschlossen werden.


§ 2: Aufgaben des Organisationstreffens
---------------------------------------

Das Organisationstreffen hat folgende Aufgaben:

a) Vorschläge über Aufnahme und Ausschluss von Vereinsmitgliedern;

b) Beschlussfassung über Vereinsordnungsänderungen;

c) Beratung und Beschlussfassung über sonstige Fragen.
